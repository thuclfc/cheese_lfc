/*!
 * Cheese
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2019. MIT licensed.
 */$(document).ready(function () {
    var height_scroll = $('body').innerHeight();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('header').addClass('is-scroll');
        } else {
            $('header').removeClass('is-scroll');
        }
    });

    // show hidden button book package
    $('.package_photo .book').hover(function () {
        $(this).addClass('active');
    });
    $('.package_photo').mouseleave(function () {
        $('.package_photo .book').removeClass('active');
    });
});

function tabs(tab, tab_content, tab_active, index_active) {
    $(tab_content).addClass('hidden_tab');
    $(tab).eq(tab_active).addClass('active');
    $(tab_content).eq(index_active).removeClass('hidden_tab').addClass('show_tab');
    $(tab).click(function () {
        $(tab).removeClass('active');
        $(this).addClass('active');
        var tab_index = $(this).index();
        $(tab_content).addClass('hidden_tab').removeClass('show_tab');
        $(tab_content).eq(tab_index).removeClass('hidden_tab').addClass('show_tab');
    });
}