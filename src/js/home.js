$(document).ready(function () {
    // call slider plugin slick.js
    $('.slider_album_week').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '78px',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,

                    centerPadding: '50px',
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    centerPadding: '20px',
                }
            }
        ]
    });
    $('.list_album_author').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
    });

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        arrows: false,
        centerMode: true,
        centerPadding: 0,
        focusOnSelect: true
    });

    // call function tab
    tabs('.album__tab--love li','.album_list_love',0,0);
    tabs('.album__tab--clothes li','.album_list_clothes',0,0);

    //
    /*$("#popup-clothes").modal('show');*/

    
});